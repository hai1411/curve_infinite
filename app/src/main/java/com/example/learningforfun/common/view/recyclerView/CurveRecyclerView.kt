package com.example.learningforfun.common.view.recyclerView

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.cos

class CurveRecyclerView : RecyclerView {

    constructor(context: Context) : super(context)

    constructor(context: Context,attrs : AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?,defStyleAttr : Int) : super(context,attrs,defStyleAttr)

    //All information to create a curve recyclerView Foreground
    private val circleOffset = 700
    private val mDegreeToRadian = Math.PI.toFloat() / 180f //To help convert from degree to radian
    private val translationRatio = 10f

    //All information to create a curve recyclerView Background
    private lateinit var mPaint : Paint
    private lateinit var mRectF: RectF

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        val itemCount = childCount
        for (i in 0 until itemCount) {
            val view = getChildAt(i)
            applyToView(view)
        }
    }

    override fun requestLayout() {
        super.requestLayout()
        if (layoutManager != null){
            val itemCount = layoutManager?.childCount
            for (i in 0 until itemCount!!) {
                val view = getChildAt(i)
                applyToView(view)
            }
        }
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        val centerSnapHelper = CenterSnapHelper((adapter as ListAdapter<*, *>).currentList.size - 2)
        centerSnapHelper.attachToRecyclerView(this)
        this.smoothScrollToPosition(5)
    }

    /**
     * This function calculate how a View or an Item in RecyclerView should be rotate and translate
     */
    private fun applyToView(view: View) {
        val viewHalfWidth = view.width / 2f //Haft width of an item in RecyclerView (a View)
        val recyclerviewHaftWidth = this.width / 2f //Haft width of the recyclerView
        val x = view.x //x of the view
        val rot = recyclerviewHaftWidth - viewHalfWidth - x
        val distanceToTranslate = (1.08f - cos((rot / translationRatio) * mDegreeToRadian)) * circleOffset
        view.translationY = distanceToTranslate
    }

    override fun onDraw(c: Canvas?) {
        super.onDraw(c)
        drawArcBackGround(c)
    }

    private fun drawArcBackGround(c : Canvas?) {
        mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        mRectF = RectF()
        if (this.height != 0 && this.width != 0) {
            mPaint.color = Color.parseColor("#0D000000")
            mRectF.top = 0f
            mRectF.bottom = this.height.toFloat() + this.height.toFloat()
            mRectF.left = -(this.height / 10f)
            mRectF.right = this.width + (mRectF.left * -1)
            c?.drawArc(mRectF,180f,180f,false,mPaint)
        }
    }
}