package com.example.learningforfun.common.view

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.learningforfun.adapter.LabelAdapter

class MyRecyclerView: RecyclerView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet?) : super(context,attributeSet)

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr : Int) : super(context,attributeSet,defStyleAttr)

    /**
     * Override function setAdapter
     * @param adapter : Adapter for RecyclerView
     * @return void
     */
    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        //After set adapter for RecyclerView, adapter will submit the inputList
        //So can the layout calculate how many item are visible on the recyclerView
        //After the recyclerView finish initialize
        post {
            //This is a current list data of Adapter
            var currentList = (adapter as ListAdapter<*, *>).currentList
            val lastVisiblePosition = (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
            if (currentList.size - 1 <= lastVisiblePosition) {
                //If the last visible position smaller or equals size of currentList
                //do nothing
            } else {
                /* Else: x3 the currentList
                 * Pros:
                 *  Make the calculation when scroll infinite much more smooth (reduce lag when scroll)
                 * Cons:
                 *  May affect to perform if there to much item
                 */
                val newList = currentList + currentList + currentList
                adapter.submitList(newList as List<Nothing>?)
            }
        }
    }

    /**
     * This in function handle onScroll event of recyclerView
     * @return void
     */
    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)
        //Because our recyclerView is HORIZONTAL
        //So we only care to handle the @param dx
        //This is current list of Adapter
        val currentList = (this.adapter as LabelAdapter).currentList
        //When User scroll, we get the first position visible
        val firstPositionVisible = (this.layoutManager as LinearLayoutManager)
            .findFirstVisibleItemPosition()
        //dx != mean user start scroll HORIZONTAL
        if (dx != 0 && firstPositionVisible != NO_POSITION) {
            // First part of the List from first visible pos to size of list
            val firstPart = currentList.subList(firstPositionVisible, currentList.size)
            // Second part of the List from first item pos to first visible pos
            val secondPart = currentList.subList(0, firstPositionVisible)
            //Combine to part then submit the list
            val totalList = firstPart + secondPart
            (adapter as LabelAdapter).submitList(totalList)
        }
        //dy != 0 mean user start scrolled VERTICALLY
        else if (dy != 0 && firstPositionVisible != NO_POSITION) {
            val firstPart = currentList.subList(firstPositionVisible, currentList.size)
            // Second part of the List from first item pos to first visible pos
            val secondPart = currentList.subList(0, firstPositionVisible)
            //Combine to part then submit the list
            val totalList = firstPart + secondPart
            (adapter as LabelAdapter).submitList(totalList)
        }
    }
}