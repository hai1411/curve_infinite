package com.example.learningforfun.common.view.recyclerView

import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView

class CenterSnapHelper(private val numberOfItem : Int) : LinearSnapHelper() {

    override fun calculateDistanceToFinalSnap(
        layoutManager: RecyclerView.LayoutManager,
        targetView: View
    ): IntArray? {
        return super.calculateDistanceToFinalSnap(layoutManager, targetView)
    }

    override fun findSnapView(layoutManager: RecyclerView.LayoutManager?): View? {
        if (layoutManager is LinearLayoutManager) {
            val lastPos = layoutManager.findLastVisibleItemPosition()
            if (lastPos <= 5) {
                return layoutManager.findViewByPosition(3)
            }
            else if (lastPos - 1 == numberOfItem) {
                return layoutManager.findViewByPosition(numberOfItem - 2)
            }
        }
        return super.findSnapView(layoutManager)
    }

}