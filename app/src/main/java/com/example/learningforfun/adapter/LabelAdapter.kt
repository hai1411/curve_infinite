package com.example.learningforfun.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.learningforfun.R
import com.example.learningforfun.databinding.LabelItemBinding
import com.example.learningforfun.model.Label

class LabelAdapter : ListAdapter<Label, LabelAdapter.LabelItemViewHolder>(LabelDiffCallBack()) {

    var itemLabelSelected : ((Label) -> Unit)? = null

    fun setData(labelList : List<Label>) {
        if (labelList.size % 5 == 0
            || (labelList.size + 1) % 5 == 0
            || (labelList.size + 2) % 5 == 0
            || (labelList.size + 3) % 5 == 0 ) {
            (labelList as ArrayList).add(0, Label("Null"))
            labelList.add(Label("Null"))
        }
        this.submitList(labelList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LabelItemViewHolder {
        val view = LabelItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return LabelItemViewHolder(view, this::onLabelSelected)
    }

    override fun onBindViewHolder(holder: LabelItemViewHolder, position: Int) {
        holder.itemView.rootView.isVisible = getItem(position).content != "Null"
        holder.bind(getItem(position))
    }

    private fun onLabelSelected(position: Int) {
        itemLabelSelected?.invoke(currentList[position])
    }

    inner class LabelItemViewHolder(
        private val binding : LabelItemBinding,
        private val cb: (Int) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(label : Label) {
            binding.txvContent.text = label.content
        }

        init {
            binding.root.setOnClickListener {
                cb.invoke(adapterPosition)
            }
        }
    }
}

class LabelDiffCallBack : DiffUtil.ItemCallback<Label>() {
    override fun areItemsTheSame(oldItem: Label, newItem: Label): Boolean {
        return oldItem.content == newItem.content
    }

    override fun areContentsTheSame(oldItem: Label, newItem: Label): Boolean {
        return oldItem == newItem
    }
}