package com.example.learningforfun

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.learningforfun.adapter.LabelAdapter
import com.example.learningforfun.common.view.recyclerView.CenterSnapHelper
import com.example.learningforfun.databinding.ActivityMainBinding
import com.example.learningforfun.model.Label
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var labelAdapter: LabelAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        initAction()
    }

    private fun initView() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initLabelRecyclerView()

        binding.bottomAppBarView.apply {
            background = null
            menu.getItem(2).isEnabled = false
        }

        binding.bottomAppBar.apply {
            background = getDrawable(R.drawable.custom_background)
        }
    }

    private fun initAction() {
        labelAdapter?.itemLabelSelected = {
            Log.d("Hai", it.content)
        }

        binding.tab.setOnClickListener {

        }

        binding.bottomAppBarView.setOnItemSelectedListener {

            return@setOnItemSelectedListener true
        }
    }

    private fun initLabelRecyclerView() {
        val labelList = ArrayList<Label>()
        for (i in 1..19) {
            labelList.add(Label("Hello$i"))
        }
        labelAdapter = LabelAdapter()
        labelAdapter?.setData(labelList)
        binding.rclLabel.layoutManager = LinearLayoutManager(applicationContext, RecyclerView.HORIZONTAL, false)
        binding.rclLabel.adapter = labelAdapter
    }
}

